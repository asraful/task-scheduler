This Java application is about to run several tasks(stand alone or dependent) onto a remote network agent using concurrency. The network component is developed using Java Socket API. The application is smart enough to detect any remote agent failure. At which point, the system is capable enough to run the tasks locally.

Input:
------
	TaskFlow:	T1->T2->(T3|T4)->T5
	Number of threads: N
	where,
		'->' means precedes
		'|' means tasks can be run in parallel

	I have represented the above task flow in the following format: 

		taskName task param dependentTask
		
		T1 sleep 10 
		T2 sum 10000000 T1		
		T3 sleep 10 T2
		T4 sleep 15 T2
		T5 sum 1000000000 T3 T4 	

	For the sake of simplicity, all the task here do very dumb kind of tasks; like; Sleep and Sum. Can be extended to any meaningful task definition.


Output:
-------
	1) Each Task's execution time
	2) Total system time


How to Run:
------------
	Be sure to put the input file 'Input' in the same directory. This contains the task flow.

	1) Run the remote agent onto the remote server. In this case it is the Server package
		- java -jar dist/Server.jar 2345
		Here 2345 is the port number

	2) Run the local agent or Client. In this case it is hw3 package
		- java -jar dist/hw3.jar 3 1 localhost 2345
		Here
			3 --> number of worker threads
			1 --> Running strategy, 0=local/1=remote
			localhost --> Remote agent's host name
			2345 --> Remote agent's corresponding port number



Development approach:
---------------------
1) Each task has been treated as a Node in a LinkedList. 

2) A dependency graph data structure has been made to represent the task flow/graph

3) Each Node in the graph has two links(node list); one to its immediate predecessor list and another one to its successor list

4) A Node with no predecessor is a root node. A separate list has been used to track all the root nodes. A dependency graph like this, there might have multiple root nodes, meaning multiple available tasks who are not dependent on any other

5) If root node list is empty, the graph does not have any independent node, meaning a cyclic graph. At which point the system exists. Need to recheck the input task flow 

6) BFS algorithm is used to traverse the graph. During traversal, if an already visited node is found, there must be a cycle in the graph

7) A node with empty successor list is leaf node, at which point, the program is about to terminate

8) To maintain task execution order, Producer-Consumer technique has been used

9) Starting from the root, producer pushes a ready task into a Queue and notify all consumers. A consumer then takes a ready task from the Queue and runs it locally or in remote agent, based on the current running strategy. All the consumers are actually concurrent worker Threads

10) A consumer monitors the execution of a task. If remote agent fails, then it tries to run the task locally 

11) Consumer waits if Queue becomes empty

12) Producer waits if all the predecessor nodes of next runnable task is not yet done

13) If there is no available tasks to push and consumer has finished executing previous tasks, producer exits and set a flag; upon which consumer terminates themselves

14) For network communication, a package named SimpleMessagingArchitecture has been used. This package uses Java Socket programming at its core

