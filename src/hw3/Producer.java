/*
 * @author Asraful
 * This class provides a thread which pushes task(s) to Queue and notify worker threads
 */

package hw3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.HashSet;

class Producer extends QueueBase implements Runnable{            
    private String threadId;
    private List<Node> nodes;
    
    public Producer(Set<Node> rootNodes){
        nodes = new ArrayList<>();
        nodes.addAll(rootNodes);
    }
    
    
    @Override   
    public void run(){
        
        threadId = Thread.currentThread().getName();            
        //System.out.println(threadId+" is Starting..");            
          
        sleep(100);
        
        //List<Node> nodes = new ArrayList<>();
        //nodes.addAll(Graph.getRootNodeSet());        
        
        List<Node> nodesToRemove = new ArrayList<>();
        Set<Node> successorList = new HashSet<>();
        
        List<Node> currRunningNodes = new ArrayList<>();
        currRunningNodes.addAll(nodes);
        
        while(!nodes.isEmpty()){                        
            
            for(int i=0; i<nodes.size(); i++){
                Node n = nodes.get(i);
                
                if(n.getStatus().equals("idle") && (n.getPredecessor().isEmpty() || allPredDone(n))){                    
                    synchronized(queue){
                        enQueue(n, threadId);
                        queue.notifyAll();                            
                    }
                    successorList.addAll(n.getSuccessor());
                    
                    nodesToRemove.add(n);
                                        
                }
                                
            }//for            
            
            //Remove those nodes that are already sent to the queue
            for(Node n1 : nodesToRemove){                
                nodes.remove(n1);
            }
            nodesToRemove.clear();
                                                
            
            if(!successorList.isEmpty()){
                nodes.addAll(successorList);
                successorList.clear();
                
                currRunningNodes.clear();
                currRunningNodes.addAll(nodes);
            }
                                    
            sleep(100);
            
        }//while
        
        
        while(true){
            int i=0;
            for(Node n : currRunningNodes){
                if(!n.getStatus().equals("done")){
                    break;
                    //if(!n.getStatus().equals("notaskdef"))
                        //break;
                }
                i++;
            }
            
            if(i==currRunningNodes.size())  break;            
            else    sleep(100);
            //else    wait(100);
        }
        
        
        System.out.println();
        System.out.println("No more tasks left...");
        NO_MORE_TASKS = true;            
        
        synchronized(queue){              
            queue.notifyAll();
        }
                                                
//        System.out.println();                    
//        System.out.println("Total System Run Time: " + Utility.getSystemRunTime() + " s");
//        System.out.println();            
                
        
        
    }//run
            
    
    
    boolean allPredDone(Node node){
        for(Node n : node.getPredecessor()){
            if(!n.getStatus().equals("done")){
                //return false;
                if(!n.getStatus().equals("notaskdef"))
                    return false;
            }
        }
        return true;
    }//allPredDone
    
}
