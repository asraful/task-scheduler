
package hw3;

public class TaskExecutor implements TaskVisitor{
    
    @Override
    public Node doSum(Sum sum){
        long N = sum.getN();
        long res = 0;
        for(long i=1; i<=N; i++)
            res += i;        
        sum.setOutput(String.valueOf(res));        
        return sum.getNode();
    }

    @Override
    public Node doSleep(Sleep sleep) {
        long st = sleep.getSleepTime();
        try{
            Thread.sleep(st * 1000);
        }catch(InterruptedException e){
            System.out.println(e.toString());
        }               
        sleep.setOutput("slept for " + st +"s");
        return sleep.getNode();
    }
    
}
