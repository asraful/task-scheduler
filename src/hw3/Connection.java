
package hw3;

import comp439.hw3.sma.*;

public class Connection {
    
    private MessageClient conn;
    
    public boolean connect(String host, int port){
        try {
            conn = new MessageClient(host, port);
        } catch(Exception e) {
            System.err.println(e);            
            return false;
        }
        return true;
    }
    
    public MessageClient getConn(){
        return conn;
    }
    
    public void disconnect(){
       conn.disconnect();
    }
    
    
    
}
