
package hw3;

import java.util.Date;

class Consumer extends QueueBase implements Runnable{
    String threadId;
    Node node;
    boolean LOCAL_RUN = false;
    private final int DEFAULT_STRATEGY;
    private static int STRATEGY;
    
    public Consumer(int s){
        DEFAULT_STRATEGY = s;
        STRATEGY = DEFAULT_STRATEGY;
    }
    
    @Override
    public void run(){
        threadId = Thread.currentThread().getName();                    
        
        while(!NO_MORE_TASKS){                            
            synchronized(queue){
                
                while(queue.isEmpty() && !NO_MORE_TASKS){                   //Wait till queue is empty                    
                    wait(100);
                }

                if(!queue.isEmpty())
                    node = deQueue();   
                    
                else if(NO_MORE_TASKS)                      //Halt, if all tasks of TaskGraph are done
                    break;

                queue.notifyAll();
                
            }

            
            if(node != null){                                                    
                node.setStatus("starting");
                String st = Utility.getCurrentTime();                                
                                
                if(STRATEGY == 1)   onRemote();
                
                if(STRATEGY == 0)   onLocal();
                
                /*System.out.println("Running " + node.getTaskName() + " on remote agent, " + Utility.getAgent());                                                
                try{
                    Context context = new Context(new Remote());
                    context.execute(node);                    
                }catch(Exception ex){
                    System.out.println(ex.toString());                    
                    LOCAL_RUN = true;
                }
                
                if(LOCAL_RUN){
                    System.out.println();
                    System.out.println("Remote execution failed. Running " + node.getTaskName() +" locally.");
                    try{
                        Context context = new Context(new Local());
                        context.execute(node);
                    }catch(Exception ex){
                        System.out.println(ex.toString());                                            
                    }                    
                }*/
                
                String et = Utility.getCurrentTime();                                                
                
                System.out.println("\n" +
                        node.getTaskName() + ": " + node.getTask() + " " + node.getTaskParam() + "\n" //";  " + ((DEFAULT_STRATEGY==0)?"LOCAL":"REMOTE") + "\n"
                        + node.getTaskName() + " O/P: " + node.getOutput() + ";  RunTime: " 
                        + Utility.getTimeDiff(Utility.parseTime(Long.parseLong(node.getStartTime())), Utility.parseTime(Long.parseLong(node.getEndTime()))) + ";  Total: "
                        + Utility.getTimeDiff(Utility.parseTime(Long.parseLong(st)), Utility.parseTime(Long.parseLong(et)))
                        + "\n"
                );
                
            }

        }//while
        
    }//run
    
    
    void onLocal(){
        System.out.println("Running " + node.getTaskName() + " on LOCAL machine");
        try{
            Context context = new Context(new Local());
            context.execute(node);
        }catch(Exception ex){
            System.out.println(ex.toString());                                            
        }
        STRATEGY = DEFAULT_STRATEGY;
    }
     
    
    void onRemote(){
        System.out.println("Running " + node.getTaskName() + " on REMOTE agent, " + Utility.getAgent());
        try{
            Context context = new Context(new Remote());
            context.execute(node);
        }catch(Exception ex){
            //System.out.println(ex.toString());                    
            System.out.println();
            System.out.println("Remote execution failed");
            
            STRATEGY = 0;   //onLocal()            
        }        
        
    }
    
    
}
