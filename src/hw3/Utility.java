/**
 * @author Asraful
 * This module provides a parser and a method for cyclic graph checking
 */

package hw3;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;



class Utility {       
    
    static long startTime;          //System start time
    static long endTime;            //System end time
    
    static List<String> serverAgents = new ArrayList<String>();
    static String agent="";

    static void startSystemTime(){
        startTime = System.currentTimeMillis();
    }
    
    /*static String getSystemRunTime(){    
        endTime   = System.currentTimeMillis();        
        return String.valueOf((endTime - startTime)/1000.0000);
    }*/
    
    static double getSystemRunTime(){    
        endTime   = System.currentTimeMillis();        
        return (endTime - startTime)/1000.00;
    }
    
    
    
        
    static List<List<String>> readInputFile(String fileName){
        List<List<String>> taskArray = new ArrayList<>();                
        
        try{                       
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine().trim();
            
            int i=0;
            while(line != null){
                line = line.trim();
                if(!line.equals("") && !line.startsWith("//")){
                    List<String> taskDetail = Arrays.asList(line.split(" "));    //Save each task with its whole line
                    taskArray.add(taskDetail);                                      
                }                
                line = reader.readLine();                                                
            }
                        
        }catch(Exception e){            
            System.out.println(e.toString());       
            return null;
        }
                
        return taskArray;
    
    }//ENd readInputFile
    
    
    
    static boolean checkTaskFormat(List<List<String>> taskArray){
        for(List<String> eachLine : taskArray){
            if(eachLine.size()<3){
                System.out.println(eachLine + " has insufficient data. A task should contain atleast 'taskName' 'task' 'parameter'");
                return false;
            }            
        } 
        return true;        
    }//End of checkTaskFormat

    
    
    
    static void populateServerAgents(String fileName){
        try{                       
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            String line = reader.readLine().trim();                                    
            
            while(line != null){
                line = line.trim();
                if(!line.equals("") && !line.startsWith("//")){
                    List<String> tokens = Arrays.asList(line.split(" "));    //Save each task with its whole line
                    String host = tokens.get(0).trim();
                    String port = tokens.get(1).trim();
                    //serverAgents.add(host + ":" + port);                        
                    agent = host + ":" + port;
                    break;
                }                
                line = reader.readLine();
            }            
                        
        }catch(Exception e){            
            System.out.println(e.toString());                   
        }
    }//End of populateServerObject
    
        
            
    static String getAnAgent(int index){
        return serverAgents.get(index);
    }
            
    static String getCurrentTime(){
        //return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss::SSSS").format(new Date(System.currentTimeMillis()));
        return "" + System.currentTimeMillis();
    }
    
    static String getTimeDiff(String start, String stop){        
        String out = "";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss::SSSS");
        
        try{
            Date d1 = format.parse(start);
            Date d2 = format.parse(stop);
                        
            long diffMiliSec = d2.getTime() - d1.getTime();            
            if(diffMiliSec < 1000)
                out += diffMiliSec + "ms";
            else
                out += (diffMiliSec/1000) + "s" + " " + (diffMiliSec%1000) + "ms";
                        
            return out;
            
        }catch(Exception ex){
            System.out.println(ex.toString());
            return "";
        }
    }
    
    static String parseTime(long dt){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss::SSSS").format(new Date(dt));
    }
    
    
    static void setAgent(String host, String port){
        agent = host + ":" + port;
    }
    static String getAgent(){
        return agent;
    }
            
}
