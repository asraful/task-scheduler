
package hw3;

import java.util.Set;

public class Concurrency {    
    private Set<Node> rootNodeSet;
    private static int NUM_OF_WORKER_THREADS;
    private static int DEFAULT_STRATEGY;    // 0 = LOCAL; 1 = REMOTE
    
    
    public void doRun(){
       Thread producer = new Thread(new Producer(rootNodeSet));         //Producer Thread, to push task to the queue
       producer.setName("Producer");       
                     
       for(int i=0; i<NUM_OF_WORKER_THREADS; i++){
           Thread consumer = new Thread(new Consumer(DEFAULT_STRATEGY));        //Consumer takes a task from queue and execute it
           consumer.setName("Consumer-" + i);           
           consumer.start();
       }       
       
       producer.start();
       
       try{
           producer.join();
       }catch(Exception e){
       }
       
    }
    
    
    public void setWorkerThreadsCount(int n){
        NUM_OF_WORKER_THREADS = n;
    }
    
    public void setRootNodeList(Set<Node> s){
        rootNodeSet = s;
    }
    
    public void setDefaultStrategy(int strategy){
        DEFAULT_STRATEGY = strategy;
    }
    
    
}
