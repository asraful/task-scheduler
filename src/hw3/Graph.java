
package hw3;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Queue;
import java.util.LinkedList;


class Graph {
    static private Set<Node> rootNodesSet;                      //Holds all the nodes who have no predicessor
    static private HashMap<String, Node> nodeMap;               //Map node name with its corresponding node object    
    
    
    Graph(){
        rootNodesSet = new HashSet<>();
        nodeMap = new HashMap<>();        
    }
        
        
    //Make a graph of nodes
    boolean createGraphNew(List<List<String>> taskArray){        
        Node node;
        try{
            if(taskArray.isEmpty()) return false;
                
            for(List<String> eachTask : taskArray){

                String taskName = eachTask.get(0);
                String task = eachTask.get(1);
                long taskParam = Long.parseLong(eachTask.get(2));
                List<String> predList = new ArrayList<>();
                if(eachTask.size() > 3){
                    for(int i=3; i<eachTask.size(); i++)
                        predList.add(eachTask.get(i));
                }


                if(nodeMap.containsKey(taskName)){                              //Already existed
                    node = nodeMap.get(taskName);
                    node.setTask(task);
                    node.setTaskParam(taskParam);

                    if(predList.isEmpty() && node.getPredecessor().isEmpty()){                                      //If it does not have any predecessor list then it is independent task
                         rootNodesSet.add(node);                                //and save ot to the independent task list                     
                    }
                    else{                                                       //Has some predecessor nodes
                        Node pred;
                        for(String predTaskName : predList){                    //For each task in predecessor list
                            if(nodeMap.containsKey(predTaskName))          
                                pred = nodeMap.get(predTaskName);               //if its already in the hashmap, make a link to that one
                            else{
                                pred = makeNode(predTaskName);                      //else create a new node
                                nodeMap.put(predTaskName, pred);
                            }

                            node.setPredecessor(pred);                          //Make both way link
                            pred.setSuccessor(node);

                        }

                        if(rootNodesSet.contains(node))                         //If previously this node was root, then remove it from that list
                            rootNodesSet.remove(node);

                    }

               }

               else{                                                             //New task
                    node = makeNode(taskName, task, taskParam);               
                    nodeMap.put(taskName, node);                

                    if(predList.isEmpty()){                                      //If it does not have any predecessor list then it is independent task
                         rootNodesSet.add(node);                                 //and save ot to the independent task list                     
                    }

                    else{
                        Node pred;
                        for(String predTaskName : predList){                    //For each task in predecessor list
                            if(nodeMap.containsKey(predTaskName))          
                                pred = nodeMap.get(predTaskName);               //if its already in the hashmap, make a link to that one
                            else{                                     
                                pred = makeNode(predTaskName);                      //else create a new node 
                                nodeMap.put(predTaskName, pred);
                            }

                            node.setPredecessor(pred);                          //Make both way link
                            pred.setSuccessor(node);                       
                        }

                    }                

               }

            }//End of For loop
            
        }catch(Exception ex){
            System.out.println(ex);
            return false;
        }
        
        return true;
        
    }//End of createGraphNew
    
    
    
    
    //Traverse the graph in BFS order and show them up
    void traverseGraph(){
        Node node;        
        Queue<Node> queue = new LinkedList<>();                       
        
        System.out.println("Graph View:");
                        
        for (Iterator<Node> it = rootNodesSet.iterator(); it.hasNext();) {      //Show all nodes of the rootNodeSet
            node = it.next();
            queue.add(node);                                
        }                        
        
        
        int qsize;
        Set<Node> succ = new HashSet<>();;
        
        while(!queue.isEmpty()){            
            qsize = queue.size();            
                                    
            for(int i=0; i<qsize; i++){
                node = queue.poll();
                node.setVisit(true);
                
                System.out.print(node.getTaskName()+ " ");
                
                succ.addAll(node.getSuccessor());                //Found the successor list of each rootNode, then their successor and then theirs and so on
            }            
            
            
            if(!succ.isEmpty()){                                
                for(Node n : succ){                        //For each successor node check its corresponding predecessor nodes.
                    if(isAllPredVisited(n)){                //If they are all visited, then push the successor node to the Queue
                        queue.add(n);                                                                                 
                    }                    
                }
                
                succ.clear();
            }
            
            System.out.println();
        }               
        
                
        resetGraphVisit();                      //Once you have traverse the graph, be sure to reset the visit status of each node
        
        System.out.println();
        
    }//End of traverseGraph
                           
    
    
    
    //Check for any possible cyclic chain using BFS algo
    boolean isCyclic(){
        
        if(nodeMap.isEmpty())   return false;
        
        if(rootNodesSet.isEmpty())  return true;                //If there is no root node, obviously the graph is cyclic
        
        Node node;        
        Queue<Node> queue = new LinkedList<>();                
        
        System.out.println("Graph View:");
        
        //Put the set of rootNode into queue. Then find their successors and put them also into Queue
        for (Iterator<Node> it = rootNodesSet.iterator(); it.hasNext();) {
            node = it.next();
            queue.add(node);            
        }
        
        int qsize;
        Set<Node> succ = new HashSet<>();;
        
        while(!queue.isEmpty()){
            qsize = queue.size();            
                                    
            for(int i=0; i<qsize; i++){
                node = queue.poll();
                
                if(node.getVisit()){                       //If the current node already visited previously, then its a cycle                    
                    resetGraphVisit();                                        
                    System.out.println(node.getTaskName());
                    return true;
                }
                else{
                    node.setVisit(true);                   //Else update its visit status
                    System.out.print(node.getTaskName()+ " ");
                }              
                
                succ.addAll(node.getSuccessor());         
            }                        
            
            if(!succ.isEmpty()){                
                for(Node n : succ){                    //For each successor node, add them to the queue
                    queue.add(n);                                                     
                }
                succ.clear();
            }
            
            System.out.println();
                        
        }        
        
        resetGraphVisit();
        
        System.out.println();
        
        return false;
        
    }//End of isCyclic()
    
    
    
    
    //Display taskName to their corresponding node
    void showNodeMap(){
        System.out.println("Task Mapping:");
        for(String key : nodeMap.keySet()){
            System.out.print(key + " -> ");
            List<Node> adjList = nodeMap.get(key).getSuccessor();
            for(Node n : adjList){
                System.out.print(n.getTaskName() + ", ");
            }
            System.out.println();
        }
        System.out.println();
    }//End of showNodeMap
    
    
 
    
    //Check if all nodes are visited in the graph
    boolean isAllNodeVisited(){
        for(String taskName : nodeMap.keySet()){
            if(!nodeMap.get(taskName).getVisit())
                return false;        
        }
        
        return true;
        
    }//End of isAllNodevisited
    
   
    
    //Check to see if all predecessor of a given node are already visited or not
    boolean isAllPredVisited(Node node){        
        for(Node n : node.getPredecessor()){            
            if(!n.getVisit())
                return false;
        }
        return true;        
    }//End of isAllPredVisited()
    
    
    
    
    //Reset the visit status of all node
    void resetGraphVisit(){    
        for(String nodeId : nodeMap.keySet()){
            nodeMap.get(nodeId).setVisit(false);
        }                      
    }//End of resetGraphVisit()
    
   

        
    public Set<Node> getRootNodeSet(){
        return rootNodesSet;
    }          
    public HashMap<String, Node> getNodeMap(){
        return nodeMap;
    }          
    
    
    
    //Making a Node
    Node makeNode(String taskName){
       Node node = new Node(taskName);       
       return node;
    }
    
    Node makeNode(String taskName, String task, long taskParam){
       Node node = new Node(taskName);
       node.setTask(task);
       node.setTaskParam(taskParam);
       return node;
    }
    //
    
    
}
