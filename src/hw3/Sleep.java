/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hw3;

/**
 *
 * @author Asraful
 */
public class Sleep implements Task{
    
    protected final Node node;
        
    public Sleep(Node node){
        this.node = node;
    }
    
    public long getSleepTime(){
        return node.getTaskParam();        
    }
    
    public Node getNode(){
        return this.node;
    }
    
    public void setOutput(String str){
        node.setOutput(str);
    }
    
    @Override
    public Node accept(TaskVisitor visitor) {
        return visitor.doSleep(this);
    }
    
}
