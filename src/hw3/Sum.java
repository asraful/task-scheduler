
package hw3;

public class Sum implements Task{
        
    protected Node node;
       
    public Sum(Node node){
        this.node = node;
    }
        
    public Node getNode(){
        return this.node;
    }
    
    public long getN(){
        return node.getTaskParam();
    }
      
    public void setOutput(String str){
        node.setOutput(str);
    }
    
    @Override
    public Node accept(final TaskVisitor visitor) {
        return visitor.doSum(this);
    }
        
}
