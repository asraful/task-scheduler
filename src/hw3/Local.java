
package hw3;

public class Local implements Strategy{

    @Override
    public void execute(Node node) throws Exception{
        //node.setStatus("starting");
        
        String taskRoutine = node.getTask();        
        
        if(taskRoutine != null){
            
            node.setStatus("running");
            node.setStartTime(Utility.getCurrentTime());            
            
            switch(taskRoutine){
                case "sleep":                                                                                                    
                    new Sleep(node).accept(new TaskExecutor());                 
                    break;
                
                case "sum":                                        
                    new Sum(node).accept(new TaskExecutor());                    
                    break;                
                
                default:
                    System.out.println("No available task definition found for " + node.getTaskName());
                    //node.setStatus("notaskdef");
                    node.setEndTime(Utility.getCurrentTime());
                    node.setStatus("done");                    
                    return;
            }                        
            
            node.setEndTime(Utility.getCurrentTime());
            node.setStatus("done");
            
        }
        else{
            System.out.println("Empty task " + node.getTaskName());
        }
        
    }
    
}
