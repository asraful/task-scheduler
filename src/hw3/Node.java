
package hw3;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Node {
    private String taskName;    
    private String status;              //'idle', 'starting', 'running', 'done', 'notaskdef'
    private boolean visited;
    private List<Node> predecessorList;
    private List<Node> successorList;
    private String task;
    private long taskParam;
    private String startTime;
    private String endTime;
    private String output;    
    
    Node(){
        this.taskName = null;        
        status = "idle";
        visited = false;
        predecessorList = null;
        successorList = null;
        task = null;
        taskParam = 0;
        startTime = "";
        endTime = "";
        output = "";
    }
    
    Node(String name){
        this.taskName = name;        
        status = "idle";
        visited = false;
        predecessorList = new ArrayList<>();
        successorList = new ArrayList<>();
        task = null;
        taskParam = 0;
        startTime = "";
        endTime = "";
        output = "";
    }
    
    void setTaskName(String taskName){
        this.taskName = taskName;
    }
    String getTaskName(){
        return this.taskName;
    }
    
    
    void setStatus(String status){
        this.status = status;
    }
    String getStatus(){
        return this.status;
    }
    
    void setVisit(boolean visited){
        this.visited = visited;
    }
    boolean getVisit(){
        return this.visited;
    }
    
    void setPredecessor(Node node){        
        this.predecessorList.add(node);
    }
    void setPredecessor(Set<Node> node){
        this.predecessorList.addAll(node);
    }
    List<Node> getPredecessor(){
        return this.predecessorList;
    }
    
    void setSuccessor(Node node){
        this.successorList.add(node);
    }
    List<Node> getSuccessor(){
        return this.successorList;
    }
    
    void setTask(String task){
        this.task = task;
    }
    String getTask(){
        return this.task;
    }
    
    void setTaskParam(long taskParam){
        this.taskParam = taskParam;
    }
    long getTaskParam(){
        return this.taskParam;
    }
        
    void setStartTime(String st){
        startTime = st;
    }
    
    String getStartTime(){
        return startTime;
    }    
    
    void setEndTime(String et){
        endTime = et;
    }
    
    String getEndTime(){
        return endTime;
    }
    
    void setOutput(String str){
        output = str;
    }
    String getOutput(){
        return output;
    }
    
}
