
package hw3;

public class Context {
    private Strategy strategy;
    
    public Context(Strategy strategy){
        this.strategy = strategy;
    }
    
    public void execute(Node node) throws Exception{
        this.strategy.execute(node);
    }
}
