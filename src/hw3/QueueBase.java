/**
 * @author Asraful
 * Provides some common behaviors to both QueueFiller and TaskExecutor threads
 */

package hw3;

import java.util.List;
import java.util.LinkedList;
import java.util.Queue;

class QueueBase {
    static Queue<Node> queue;
    static boolean NO_MORE_TASKS;                       //Flag to tell worker threads to Halt. Only updated by QueueFiller thread
    
    
    QueueBase() {    
        queue = new LinkedList<>();
        NO_MORE_TASKS = false;        
    }
  
    
    void enQueue(Node node, String threadId){            
        //System.out.println(threadId + " is producing: " + node.getTaskName());
        queue.add(node);                
    }//enQueue
    
    
    Node deQueue(){
        return queue.poll();
    }
    
    
    void sleep(int st){
        try{
            Thread.sleep(st);
        }catch(InterruptedException e){
            System.out.println(e.toString());
        }
    }
    
    void wait(int wt){
        try{            
            queue.wait(wt);                
        }catch(InterruptedException e){
            System.out.println(e.toString());
        }
    }        
    
    
    
    void displayQueue(){
        for(Node n : queue){
            System.out.println(n.getTaskName() + ", ");
        }
    }
    
    
}
