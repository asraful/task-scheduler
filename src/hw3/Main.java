
package hw3;

import java.util.ArrayList;
import java.util.List;

public class Main {

   public static void main(String[] args) {              
       Utility.startSystemTime();                 //Start system clock
       
       int DEFAULT_STRATEGY = 1;    // LOCAL=0, REMOTE=1                  
       int NUM_OF_WORKER_THREADS = 2;                       //Consumer Thread count; Default 2       
       
       if(args.length > 0){
           NUM_OF_WORKER_THREADS = Integer.parseInt(args[0]);
           
           DEFAULT_STRATEGY = Integer.parseInt(args[1]);
           
           String host = args[2].trim();
           String port = args[3].trim();           
           Utility.setAgent(host, port);
       }
       else{
           System.out.println("Wrong format.");
           System.out.println("Expecting: 'java -jar dist/hw3.jar num_of_threads default_running_strategy host port'");
           System.out.println("Where, default_running_strategy: 0/1; 0=LOCAL, 1=REMOTE");
           return;
       }
       
       /*if(args.length == 1)
            num_of_worker_threads = Integer.parseInt(args[0]);                   
       else if(args.length > 1){
           String host = args[1].trim();
           String port = args[2].trim();
           System.out.println(host+", "+port);
           Utility.setAgent(host, port);
       }
       else
           Utility.populateServerAgents("RemoteServerList");
       */
       
       //Read and Parse input file
       List<List<String>> taskArray;
       taskArray = Utility.readInputFile("Input");
       
       if(taskArray == null){
           System.out.println("File not found");
           return;
       }       
       
       //Show input
       System.out.println("Input:");
       for(List<String> arr : taskArray)
           System.out.println(arr);
       System.out.println();
       
       
       if(!Utility.checkTaskFormat(taskArray))  return;   
       
              
       
       Graph graph = new Graph();
       boolean success = graph.createGraphNew(taskArray);
       if(success){           
           graph.showNodeMap();
                       
           if(graph.isCyclic()){                                                               
              System.out.println("Cyclic task graph found..System Exited..");
              return;
           }
           
                      
           Concurrency concur = new Concurrency();
           concur.setWorkerThreadsCount(NUM_OF_WORKER_THREADS);
           concur.setRootNodeList(graph.getRootNodeSet());
           concur.setDefaultStrategy(DEFAULT_STRATEGY);
           concur.doRun();
           
           
           System.out.println();                    
           System.out.println("Total System Run Time: " + Utility.getSystemRunTime() + " s");
           System.out.println();            
           
       }
       else
           System.out.println("Failed to built graph");
       
       
       
       
       
       /*
       //Make a graph
       Graph graph = new Graph();
       graph.createGraphNew(taskArray);
       graph.showNodeMap();
       
       
       if(graph.isCyclic()){                                                    
            System.out.println("Cyclic task graph found..System Exited..");
            return;
       }       
       //graph.traverseGraph();               //Traverse the graph using BFS and show it up                            
              
       
       
       //Multi-Threading Starts
       Thread producer = new Thread(new Producer(Graph.getRootNodeSet()));         //Producer Thread, to push task to the queue
       producer.setName("Producer");       
                     
       for(int i=0; i<num_of_worker_threads; i++){
           Thread consumer = new Thread(new Consumer());        //Consumer takes a task from queue and execute it
           consumer.setName("Consumer-" + i);           
           consumer.start();
       }       
       
       producer.start();           
       
       //Multi-Threading Ends here             
       */
       
   
   }//End of main()   

}

