
package hw3;

import comp439.hw3.sma.*;

public class Remote implements Strategy{    

    private Connection conn;
    
    @Override
    public void execute(Node node) throws Exception{        
        
        node.setStatus("running");
        
        conn = new Connection();               
        
        //String agent = Utility.getAnAgent(0);        
        String agent = Utility.getAgent();
        String host = agent.substring(0, agent.indexOf(":"));
        String port = agent.substring(agent.indexOf(":")+1);

        boolean yes = conn.connect(host, Integer.parseInt(port));
        
        if(yes){            
            Message message = new Message();        
            message.setType(100);
            
            message.setParam("taskName", node.getTaskName());
            message.setParam("task", node.getTask());
            message.setParam("taskParam", "" + node.getTaskParam());            
            message.setParam("output", node.getOutput());                                    
              
            try{       
                message = conn.getConn().call(message);                
                conn.disconnect();                                
            }catch(Exception ex){
                System.out.println(ex.toString());
                throw new Exception();
            }
                        
            
            if(message.getParam("output") == null)
                throw new Exception();                        
            else{                
                node.setOutput(message.getParam("output"));
                node.setStatus("done");                
                node.setStartTime(message.getParam("startTime"));
                node.setEndTime(message.getParam("endTime"));                                                              
            }
                    
            //System.out.println();
            
        }
        
        else{
            throw new Exception();        
        }
        
    }        
        
    
}
