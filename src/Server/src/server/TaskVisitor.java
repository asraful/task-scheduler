
package server;

public interface TaskVisitor {
    Node doSum(Sum sum);
    Node doSleep(Sleep sleep);
}
