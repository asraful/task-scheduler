
package server;

import java.util.*;
import comp439.hw3.sma.*;
import java.text.SimpleDateFormat;

public class Server implements Deliverable{
    public static final int DATE_SERVICE_MESSAGE = 100;    
    public static final int DATE_SERVICE_PORT = 2345;
        
    public Message send(Message message) {
        String taskName = message.getParam("taskName");
        String task = message.getParam("task");
        String taskParam = message.getParam("taskParam");
        String output = message.getParam("output");
            
        //message.setParam("startTime", getCurrentTime());
  
        /*try{
            Thread.sleep(5000);
        }catch(Exception ex){
            
        }*/
        
        
        if(task != null){
            Node node = new Node(taskName);
            node.setTask(task);
            node.setTaskParam(Long.parseLong(taskParam));
            node.setOutput(output);            
            
            System.out.println(taskName + "(" + task + " " + taskParam + ") " + "starts @" + getCurrentTime());
            
            switch(task){
                case "sleep":      
                    node.setStartTime(getCurrentTime());                    
                    new Sleep(node).accept(new TaskExecutor());     
                    node.setEndTime(getCurrentTime());
                    //message.setParam("output", node.getOutput());
                    break;
                
                case "sum":                                        
                    node.setStartTime(getCurrentTime());
                    new Sum(node).accept(new TaskExecutor());
                    node.setEndTime(getCurrentTime());
                    //message.setParam("output", node.getOutput());
                    break;                
                
                default:
                    node.setStartTime(getCurrentTime());
                    node.setEndTime(getCurrentTime());
                    //message.setParam("output", "");
                    
            }                        
            message.setParam("output", node.getOutput());
            message.setParam("startTime", node.getStartTime());
            message.setParam("endTime", node.getEndTime());                        
            
            System.out.println(taskName + "(" + task + " " + taskParam + ") " + "O/P: " + node.getOutput());
            System.out.println(taskName + "(" + task + " " + taskParam + ") " + "ends @" + getCurrentTime());
            
        }
        System.out.println();
        System.out.println("Sending.. " + message);
        System.out.println();
        
        //System.out.println(message);
        
        return message;
    
    }
    
    public String getCurrentTime(){                
        return "" + System.currentTimeMillis();
    }
    
    public static void main(String[] args) {
        int port;
        if(args.length > 0) port = Integer.parseInt(args[0]);
        else    port = DATE_SERVICE_PORT;
        
        Server ds = new Server();
        ds.startServer(port);
        //ds.startServer(DATE_SERVICE_PORT);
        /*MessageServer ms;
        try {
            ms = new MessageServer(DATE_SERVICE_PORT);
        } catch(Exception e) {
            System.err.println("Could not start service " + e);
            return;
        }
        Thread msThread = new Thread(ms);
        ms.subscribe(DATE_SERVICE_MESSAGE, ds);
        msThread.start();        
        */ 
    }
    
    public void startServer(int port){
        //Server ds = new Server();
        MessageServer ms;
        try {
            ms = new MessageServer(port);
        } catch(Exception e) {
            System.err.println("Could not start service " + e);
            return;
        }
        Thread msThread = new Thread(ms);
        ms.subscribe(DATE_SERVICE_MESSAGE, this);
        msThread.start();
    }
}
